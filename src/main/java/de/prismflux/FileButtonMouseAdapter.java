package de.prismflux;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class FileButtonMouseAdapter extends MouseAdapter {

	FileDropButton p = null;
	final JFileChooser filechooser = new JFileChooser();
	FileFilter fileFilter = new ExtensionFileFilter("Audio Files", new String[] { "MP3", "WAV", "OGG" });

	public FileButtonMouseAdapter(FileDropButton parent) {
		super();
		this.p = parent;

		this.filechooser.addChoosableFileFilter(this.fileFilter);
		this.filechooser.setAcceptAllFileFilterUsed(false);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		super.mouseClicked(e);

		switch (e.getButton()) {
		case MouseEvent.BUTTON1:
			// System.out.println("Linksklick");
			this.openDialog();
			break;

		case MouseEvent.BUTTON3:
			// System.out.println("Rechtsklick");
			this.p.deleteFile();
			break;
		}
	}

	private void openDialog() {
		int returnVal = this.filechooser.showOpenDialog(this.p);

		if (returnVal == JFileChooser.APPROVE_OPTION) {
			this.p.setFile(this.filechooser.getSelectedFile());
		}
	}

}
