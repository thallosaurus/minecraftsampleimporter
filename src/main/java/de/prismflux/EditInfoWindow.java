package de.prismflux;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class EditInfoWindow extends JDialog {

	InfoButtonMouseAdapter p = null;
	JTextField name = null;
	JComboBox<String> packFmt = null;

	JButton okButton = new JButton("Ok");

	final String[] PACKVERSIONS = { "1.6.1 - 1.8.9", "1.9 - 1.10.2", "1.11 - 1.12.2", "1.13 - 1.14.4", "1.15 - 1.16.1",
			"1.16.2 - 1.16.5", "1.17+" };

	public EditInfoWindow(InfoButtonMouseAdapter parent) {
		this.p = parent;

		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		this.setTitle("Edit Infos");

		JPanel namePanel = new JPanel(new GridLayout(1,0));
		namePanel.add(new JLabel("Name"));
		this.name = new JTextField();
		this.name.setText("Unnamed Sample Pack");
		this.name.setPreferredSize(new Dimension(100, 25));
		namePanel.add(this.name);
		namePanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		this.add(namePanel);

		JPanel packFmtPanel = new JPanel(new GridLayout(1,0));
		packFmtPanel.add(new JLabel("Pack Format"));
		this.packFmt = this.createVersionPicker();
		packFmtPanel.add(this.packFmt);
		packFmtPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		this.add(packFmtPanel);

		this.okButton.addActionListener(e -> {
			this.setVisible(false);
		});

		this.add(this.okButton);

		this.setResizable(false);
		this.pack();
		this.setLocationRelativeTo(null);
	}

	private JComboBox<String> createVersionPicker() {
		JComboBox<String> jcl = new JComboBox<String>(this.PACKVERSIONS);
		jcl.setSelectedIndex(5);
		return jcl;
	}
	
	private void createPackIconInput() {
		JPanel panel = new JPanel(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
	}

	@Override
	public String toString() {
		return "{\n" + "  \"pack\": {\n" + "    \"pack_format\": " + (this.packFmt.getSelectedIndex() + 1) + ",\n"
				+ "    \"description\": \"" + this.name.getText() + "\"\n" + "  }\n" + "}";
	}
}
