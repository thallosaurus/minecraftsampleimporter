package de.prismflux;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import javax.sound.sampled.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;

public class SamplePlayerAdapter extends MouseAdapter {
    FileDropButton parent;

    public SamplePlayerAdapter(FileDropButton parent) {
        this.parent = parent;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (this.parent.hasSound()) {
            File nf = this.parent.getFile();
            String filename = nf.getName();
            String ext = filename.substring(filename.length() - 3);

            switch (ext.toUpperCase()) {
                case "WAV":
                    playWAV(nf);
                    break;

                case "MP3":
                    playMP3(nf);
                    break;

                default:
                    System.err.println("Filetype is not supported");
            }
        }
    }

    private void playMP3(File file) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Player player = new Player(fis);
            player.play();
        } catch (JavaLayerException e) {
            e.printStackTrace();
        }
    }

    private void playWAV(File file) {
        try {
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(file);
            Clip clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.start();

        } catch (LineUnavailableException lineUnavailableException) {
            lineUnavailableException.printStackTrace();
        } catch (UnsupportedAudioFileException unsupportedAudioFileException) {
            System.err.println("BETA: Only .wav Files are supported right now");
            unsupportedAudioFileException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
