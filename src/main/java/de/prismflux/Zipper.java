package de.prismflux;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import ws.schild.jave.Encoder;
import ws.schild.jave.EncoderException;
import ws.schild.jave.InputFormatException;
import ws.schild.jave.MultimediaObject;
import ws.schild.jave.encode.AudioAttributes;
import ws.schild.jave.encode.EncodingAttributes;

public class Zipper {

	public static void createZipFile(String pack_mcmeta, ArrayList<FileDropButton> files) throws IOException {
		// TODO implement Zipping Routine here

		JFileChooser jfc = new JFileChooser();
		jfc.setFileFilter(new FileNameExtensionFilter("ZIP-Archive", "zip"));
		jfc.setAcceptAllFileFilterUsed(false);
		int approve = jfc.showSaveDialog(null);

		if (approve == JFileChooser.APPROVE_OPTION) {
			String outputFile = jfc.getSelectedFile().toString();
			
			if (!(outputFile.substring(outputFile.length() - 4) == ".zip")) {
				outputFile += ".zip";
			}
			
			
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(outputFile));

			zipString(pack_mcmeta, "pack.mcmeta", zos);

			// add all audio files to this folder

			for (FileDropButton fdp : files) {
				if (fdp.hasSound()) {
					zipFile(fdp, zos);
				}
			}

			zos.flush();
			zos.close();

			JOptionPane.showMessageDialog(null, "Finished creating Resource Pack!");
		}
	}

	public static void zipString(String str, String name, ZipOutputStream zos) throws IOException {
		zos.putNextEntry(new ZipEntry(name));
		byte[] asBytes = str.getBytes();

		for (byte b : asBytes) {
			zos.write(b);
		}
		zos.closeEntry();
	}

	public static void zipFile(FileDropButton fdp, ZipOutputStream zos) throws IOException {
		String strTmp = System.getProperty("java.io.tmpdir");

		File f = convert(fdp.getFile(), new File(strTmp + "/" + fdp.getSoundName()));

		// zos.putNextEntry(new ZipEntry("/assets/sounds/note/"));
		readExternalFileIn(f, zos);
		zos.closeEntry();
	}

	private static void readExternalFileIn(File extFile, ZipOutputStream zos) throws IOException {
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(extFile));

		zos.putNextEntry(new ZipEntry("assets/minecraft/sounds/note/" + extFile.getName()));

		int in;

		while ((in = bis.read()) != -1) {
			zos.write((byte) in);
		}
		bis.close();
	}

	private static File convert(File source, File target) {

		AudioAttributes audio = new AudioAttributes();
		audio.setCodec("libvorbis");
		audio.setBitRate(320000);
		audio.setChannels(2);
		audio.setSamplingRate(44100);

		EncodingAttributes attrs = new EncodingAttributes();
		attrs.setOutputFormat("ogg");
		attrs.setAudioAttributes(audio);

		Encoder encoder = new Encoder();
		try {
			encoder.encode(new MultimediaObject(source), target, attrs);
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InputFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EncoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return target;
	}

	public static void test() {
		Encoder e = new Encoder();
		try {
			for (String encoder : e.getAudioEncoders()) {
				System.out.println(encoder);
			}
			System.out.println("--------");
			for (String format : e.getSupportedEncodingFormats()) {
				System.out.println(format);
			}
		} catch (EncoderException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
