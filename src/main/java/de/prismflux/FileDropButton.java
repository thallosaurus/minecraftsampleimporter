package de.prismflux;

import java.awt.GridLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

//import net.iharder.dnd.FileDrop;
import de.prismflux.icon.ResLoader;

@SuppressWarnings("serial")
public class FileDropButton extends JPanel {
	enum SampleType {
		BD, SNARE, HAT, BASS, BELL, FLUTE, CHIME, GUITAR, XYLOPHONE, IRONXYLO, COWBELL, DIDGERIDOO, BIT, BANJO, PLING,
		HARP
	}

	File sourceFile = null;

	JButton button = null;

	SampleType buttonType = null;

	Main parent = null;

	JLabel icon = null;

	public String zipFilePath = "/assets/sounds/note/";

	public FileDropButton(SampleType t, Main parent) {
		super();

		this.parent = parent;

		this.buttonType = t;

		this.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 10));
		// setLayout(new FlowLayout());
		this.setLayout(new GridLayout(0, 3));

		this.icon = this.getStoneIcon();
		this.add(icon);

		this.add(new JLabel(this.getTypeAsString()));
		this.button = new JButton("File");
		this.button.setPreferredSize(this.button.getSize());
		this.add(this.button);

		// this.setMaximumSize(this.getSize());

		// Add File Drag and Drop Listener
		/*
		 * new FileDrop(null, this, null, new FileDrop.Listener() {
		 * 
		 * @Override public void filesDropped(File[] files) { try {
		 * setFilePath(files[0]); } catch (IOException e) { e.printStackTrace(); } } });
		 */

		//this.addMouseListener(new FileButtonMouseAdapter(this));
		this.button.addMouseListener(new FileButtonMouseAdapter(this));
		this.icon.addMouseListener(new SamplePlayerAdapter(this));
	}

	private JLabel getStoneIcon() {
		BufferedImage img = ResLoader.loadImage("block/" + this.getIconPath());	

		Image dimg = img.getScaledInstance(25, 25, Image.SCALE_SMOOTH);
		JLabel icon = new JLabel(new ImageIcon(dimg));

		icon.setToolTipText(getBlockName());

		return icon;
	}

	public void setFile(File file) {
		this.sourceFile = file;

		this.button.setText(file.getName());
	}

	public File getFile() {
		return this.sourceFile;
	}

	public void deleteFile() {
		this.sourceFile = null;
		this.button.setText("File");
	}

	private String getIconPath() {
		return this.getTypeAsString().toLowerCase() + ".png";
	}

	public String getSoundName() {
		switch (this.buttonType) {
		case BD:
			return "bd.ogg";

		case SNARE:
			return "snare.ogg";

		case HAT:
			return "hat.ogg";

		case BASS:
			return "bassattack.ogg";

		case BANJO:
			return "banjo.ogg";

		case BELL:
			return "bell.ogg";

		case BIT:
			return "bit.ogg";

		case CHIME:
			return "icechime.ogg";

		case COWBELL:
			return "cow_bell.ogg";

		case DIDGERIDOO:
			return "didgeridoo.ogg";

		case FLUTE:
			return "flute.ogg";

		case GUITAR:
			return "guitar.ogg";

		case HARP:
			return "harp.ogg";

		case IRONXYLO:
			return "iron_xylophone.ogg";

		case PLING:
			return "pling.ogg";

		case XYLOPHONE:
			return "xylobone.ogg";
		}

		return "unknown.ogg";
		// return zipFilePath + this.getTypeAsString().toLowerCase() + ".ogg";
	}

	private String getTypeAsString() {
		switch (this.buttonType) {
		case BD:
			return "Basedrum";

		case SNARE:
			return "Snare";

		case HAT:
			return "Hat";

		case BASS:
			return "Bassline";

		case BANJO:
			return "Banjo";

		case BELL:
			return "Bell";

		case BIT:
			return "Bit";

		case CHIME:
			return "Chime";

		case COWBELL:
			return "Cowbell";

		case DIDGERIDOO:
			return "Didgeridoo";

		case FLUTE:
			return "Flute";

		case GUITAR:
			return "Guitar";

		case HARP:
			return "Harp";

		case IRONXYLO:
			return "Ironxylo";

		case PLING:
			return "Pling";

		case XYLOPHONE:
			return "Xylophone";
		}

		return "Unknown";
	}

	private String getBlockName() {
		switch (this.buttonType) {
		case BD:
			return "Stone";

		case SNARE:
			return "Sand";

		case BANJO:
			return "Hay";

		case BASS:
			return "Wood";

		case BELL:
			return "Block of Gold";

		case BIT:
			return "Block of Emerald";

		case CHIME:
			return "Packed Ice";

		case COWBELL:
			return "Soul Sand";

		case DIDGERIDOO:
			return "Pumpkin";

		case FLUTE:
			return "Clay";

		case GUITAR:
			return "Wool";

		case HARP:
			return "Any Block";

		case HAT:
			return "Glass";

		case IRONXYLO:
			return "Block of Iron";

		case PLING:
			return "Glowstone";

		case XYLOPHONE:
			return "Bone Block";
		}

		return "Block";
	}

	public boolean hasSound() {
		return !(this.sourceFile == null);
	}
}
