package de.prismflux;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class InfoButtonMouseAdapter extends MouseAdapter {
	Main parent = null;
	EditInfoWindow editWindow = null;

	public InfoButtonMouseAdapter(Main parent) {
		this.parent = parent;
		this.editWindow = new EditInfoWindow(this);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		super.mouseClicked(e);

		// open info dialog
		this.editWindow.setVisible(true);
	}

	@Override
	public String toString() {
		return this.editWindow.toString();
	}
}
