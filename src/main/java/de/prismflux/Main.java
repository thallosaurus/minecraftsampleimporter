package de.prismflux;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;

@SuppressWarnings("serial")
public class Main extends JFrame {

	ArrayList<FileDropButton> buttons = new ArrayList<>();
	InfoButtonMouseAdapter infoButtonHandler = null;

	public Main() {
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setTitle("Minecraft Sample Importer");

		for (FileDropButton.SampleType value : FileDropButton.SampleType.values()) {
			FileDropButton fdp = new FileDropButton(value, this);
			// fdp.setAlignmentX(Component.LEFT_ALIGNMENT);
			this.buttons.add(fdp);
			this.add(fdp, BorderLayout.WEST);
		}

		JButton runButton = new JButton("Create Resource Pack");
		runButton.addActionListener(e -> {
			// for (int i = 0; i < buttons.size(); i++) {
			// System.out.println(buttons.get(i).getFilePath());
			try {
				Zipper.createZipFile(this.infoButtonHandler.toString(), this.buttons);
			} catch (FileNotFoundException fileNotFoundException) {
				JOptionPane.showConfirmDialog(null, fileNotFoundException.getMessage());
				fileNotFoundException.printStackTrace();
			} catch (IOException ioException) {
				ioException.printStackTrace();
				JOptionPane.showConfirmDialog(null, ioException.getMessage());
			}
			// }
		});

		JPanel actionPanel = new JPanel();
		actionPanel.setLayout(new FlowLayout());

		JButton infoButton = new JButton("Edit Info");
		this.infoButtonHandler = new InfoButtonMouseAdapter(this);
		infoButton.addMouseListener(this.infoButtonHandler);
		actionPanel.add(infoButton);

		actionPanel.add(runButton);

		this.add(actionPanel);

		this.pack();
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		// Zipper.test();
	}

	public static void main(String[] args) {
		new Main();
	}
}
