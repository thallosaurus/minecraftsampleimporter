package de.prismflux.icon;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class ResLoader {
	private static InputStream load(String name) {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		return classloader.getResourceAsStream(name);
	}
	
	public static BufferedImage loadImage(String name) {
		InputStream resourceStream = load(name);
		BufferedImage img = null;
		try {
			//File resourceStream = FileLoader.get().load(name, ResLoader.class);
			img = ImageIO.read(resourceStream);
			resourceStream.close();
		} catch (IOException ioe) {
			JOptionPane.showMessageDialog(null, ioe.getMessage());
			ioe.printStackTrace();
		} catch (IllegalArgumentException fileNotFound) {
			JOptionPane.showMessageDialog(null, "Can't find resource " + name);
			fileNotFound.printStackTrace();
		}

		return img;
	}
}
